#!/bin/bash

# Script pour concaténer dans vidéos sans les réencoder
# Astuce donnée par Adrien Linuxtricks
# https://www.youtube.com/watch?v=b9_l3KAsDYg à 11:16

# Toutes les vidéos doivent être dans le même répertoire : ici "joindre_videos"
# Toutes les vidéos doivent avoir un nom commençant par "out"
# On peut choisir le format de vidéos souhaité par la variable "FORMAT"
# Il faut que ce soit le même que les vidéos sources

FORMAT="mov"

if [ -f resolvmov.txt ];
	then
	rm resolvemov.txt
fi

> resolvemov.txt
date=$(date +"%d-%m-%Y")

for f in $(ls *.MTS)
do
	echo file \'$f\' >> resolvemov.txt
done

# ffmpeg -f concat -i concat.txt -c copy $date.$FORMAT
# ffmpeg -i INPUT.mp4 -c:v dnxhd -profile:v dnxhr_hq -pix_fmt yuv422p -c:a pcm_s16le OUTPUT.mov
ffmpeg -i resolvemov.txt -c:v dnxhd -profile:v dnxhr_hq -pix_fmt yuv422p -c:a pcm_s16le $date.$FORMAT
echo "========================================="
echo "==      Votre vidéo est prête !!       =="
echo "========================================="
