#!/bin/bash
# sauvegarde-usb.sh

# --\\\\\--
# Défini les variables
SOURCEDIR='/home/nom_utilateur/'
DESTDIR='/media/nom_utilateur/nom_support_externe/nom_dossier'
CHEMIN='/home/nom_utilisateur/bin/exclus'
# --\\\\\--

# read -p "Voulez-vous éteindre la machine après la sauvegarde ? (o/n) :" down

# Le script présent vérifie si le support externe est connecté.
# Si c'est le cas exécute le script.

if [ ! -e "$DESTDIR" ]
then
	read -p "!!!!! La synchronisation n'a pu être effectuée, disque non branché : Ctrl + c pour fermer la fenetre"
else
	rsync -av --del --exclude-from=$CHEMIN $SOURCEDIR $DESTDIR 	
fi

# if [ $down = "o" ] ; then sudo shutdown -h now ; fi

exit 0

# --\\\\\--
