#!/bin/bash

# Installe le paquet deborphan (recherche les paquets orphelins)
sudo apt install deborphan

# Purge la liste des paquets récupérés par deborphan
sudo apt-get purge ${deborphan}

# Supprimer le cache des paquets périmés :
sudo apt autoclean

# Supprimer tout le cache :
sudo apt clean

# Supprime les paquets installés automatiquement
sudo apt autoremove

# Purge des fichiers de configurations résiduels
sudo dpkg -l | grep ^rc | awk '{print $2}' | xargs sudo dpkg -P

